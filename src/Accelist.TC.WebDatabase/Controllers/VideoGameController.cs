﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Accelist.TC.WebDatabase.Models;
using Accelist.TC.WebDatabase.Services;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Accelist.TC.WebDatabase.Controllers
{
    [Authorize(ActiveAuthenticationSchemes = "AccelistTrainingCenter")]
    [AutoValidateAntiforgeryToken]
    public class VideoGameController : Controller
    {
        public VideoGameController(VideoGameService videoGameService)
        {
            this.VideoGameService = videoGameService;
        }

        private readonly VideoGameService VideoGameService;

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(CreateVideoGameViewModel createVideoGameViewModel)
        {
            if(ModelState.IsValid == false)
            {
                return RedirectToAction("Add");
            }
            var email = HttpContext.User.Claims.Where(Q => Q.Type == ClaimTypes.Email)
                .Select(Q => Q.Value).FirstOrDefault();
            await VideoGameService.AddNewVideoGame(createVideoGameViewModel, email);
            return RedirectToAction("Index", "Player");
        }


        public async Task<IActionResult> GetVideoGameImage(int id)
        {
            var videoGameImage = await VideoGameService.GetVideoGameImageByVideoGameId(id);
            return File(videoGameImage, "image/*");
        }
        
        [HttpDelete("{videoGameId}")]
        [Route("VideoGame/Delete/{videoGameId}")]
        public async Task<IActionResult> Delete(int videoGameId)
        {
            var email = HttpContext.User.Claims
                .Where(Q => Q.Type == ClaimTypes.Email)
                .Select(Q => Q.Value)
                .FirstOrDefault();
            await VideoGameService.DeleteVideoGame(videoGameId, email);
            return RedirectToAction("Index", "Player");
        }
        
        [Route("VideoGame/Edit/{videoGameId}")]
        public async Task<IActionResult> Edit(int videoGameId)
        {
            var videoGameDetail = await VideoGameService.GetVideoGameDetail(videoGameId);
            return View(videoGameDetail);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditVideoGameViewModel editedVideoGame)
        {
            if(ModelState.IsValid == false)
            {
                return View();
            }
            await VideoGameService.UpdateVideoGame(editedVideoGame);
            return RedirectToAction("Index", "Player");
        }
    }
}
