﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Accelist.TC.WebDatabase.Models;
using System.Security.Claims;
using Accelist.TC.WebDatabase.Services;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace Accelist.TC.WebDatabase.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class LoginController : Controller
    {
        public LoginController(LoginService loginService, SecurityService securityService)
        {
            this.LoginService = loginService;
            this.SecurityService = securityService;
        }

        private readonly LoginService LoginService;
        private readonly SecurityService SecurityService;

        [AllowAnonymous]
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated == true)
            {
                return RedirectToAction("Index", "Player");
            }
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid == false)
            {
                return RedirectToAction("Index");
            }
            if (await SecurityService.IsRegisteredPlayer(loginViewModel) == false)
            {
                return RedirectToAction("Index");
            }
            await HttpContext.Authentication.SignInAsync("AccelistTrainingCenter",
                SecurityService.SetClaims(loginViewModel.Email), new AuthenticationProperties
                {
                    IsPersistent = loginViewModel.RememberMe
                });
            return RedirectToAction("Index", "Player");
        }

        [Authorize(ActiveAuthenticationSchemes = "AccelistTrainingCenter")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("AccelistTrainingCenter");
            return RedirectToAction("Index", "Login");
        }
    }
}
