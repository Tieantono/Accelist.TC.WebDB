﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Accelist.TC.WebDatabase.Services;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace Accelist.TC.WebDatabase.Controllers
{
    [Authorize(ActiveAuthenticationSchemes = "AccelistTrainingCenter")]
    public class PlayerController : Controller
    {
        public PlayerController(PlayerService playerService)
        {
            this.PlayerService = playerService;
        }

        private readonly PlayerService PlayerService;

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var email = HttpContext.User.Claims.Where(Q => Q.Type == ClaimTypes.Email)
                .Select(Q => Q.Value).FirstOrDefault();
            var playerPage = await PlayerService.GetPlayerPage(email);
            return View(playerPage);
        }
        
        [Route("Player/GetPage/{pageNumber}")]
        public async Task<IActionResult> GetPage(int pageNumber)
        {
            var email = HttpContext.User.Claims.Where(Q => Q.Type == ClaimTypes.Email)
                .Select(Q => Q.Value).FirstOrDefault();
            return Ok(await PlayerService.GetPlayerPageByPageNumber(pageNumber, email));
        }
    }
}
