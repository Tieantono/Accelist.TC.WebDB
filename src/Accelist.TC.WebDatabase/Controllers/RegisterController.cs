﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Accelist.TC.WebDatabase.Models;
using Accelist.TC.WebDatabase.Services;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Accelist.TC.WebDatabase.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class RegisterController : Controller
    {
        public RegisterController(RegisterService registerService, SecurityService securityService)
        {
            this.RegisterService = registerService;
            this.SecurityService = securityService;
        }

        private readonly RegisterService RegisterService;
        private readonly SecurityService SecurityService;

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated == true)
            {
                return RedirectToAction("Index", "Player");
            }
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            if(ModelState.IsValid == false)
            {
                return View();
            }
            var email = await RegisterService.RegisterNewPlayer(registerViewModel);
            var claimsPrincipal = SecurityService.SetClaims(email);
            await HttpContext.Authentication.SignInAsync("AccelistTrainingCenter", claimsPrincipal);
            return RedirectToAction("Index", "Player");
        }
    }
}
