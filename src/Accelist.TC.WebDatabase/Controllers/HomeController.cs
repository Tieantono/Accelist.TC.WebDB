﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Accelist.TC.WebDatabase.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if(HttpContext.User.Identity.IsAuthenticated == false)
            {
                return RedirectToAction("Index", "Login");
            }
            return RedirectToAction("Index", "Player");
        }
    }
}
