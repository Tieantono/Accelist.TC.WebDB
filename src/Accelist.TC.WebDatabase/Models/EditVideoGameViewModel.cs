﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Models
{
    public class EditVideoGameViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public DateTime ReleaseDate { get; set; }
        public IFormFile Image { get; set; }
        [Required]
        public int VideoGameId { get; set; }
    }
}
