﻿using Accelist.TC.WebDatabase.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Models
{
    public class PlayerViewModel
    {
        public string Email { get; set; }
        public List<VideoGame> VideoGames { get; set; }
        public int TotalPage { get; set; }
    }
}
