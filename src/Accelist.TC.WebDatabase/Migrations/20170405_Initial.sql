﻿CREATE TABLE Player(
    [Email] VARCHAR(64) PRIMARY KEY,
	[Password] VARCHAR(128) NOT NULL
)

CREATE TABLE VideoGame(
	VideoGameId INT PRIMARY KEY IDENTITY,
	[Name] VARCHAR(128) NOT NULL,
	[Price] DECIMAL NOT NULL,
	[ReleaseDate] DATETIME2 NOT NULL,
	[Image] VARBINARY(MAX) NOT NULL
)

CREATE TABLE VideoGameLibrary(
	VideoGameLibraryId INT PRIMARY KEY IDENTITY,
	Email VARCHAR(64) FOREIGN KEY REFERENCES Player(Email) NOT NULL,
	VideoGameId INT FOREIGN KEY REFERENCES VideoGame(VideoGameId) NOT NULL
)