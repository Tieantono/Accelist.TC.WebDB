using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Entities
{
    public class AccelistTCDbContext : DbContext
    {
        public AccelistTCDbContext(DbContextOptions<AccelistTCDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

        public virtual DbSet<Player> Player { get; set; }

        public virtual DbSet<VideoGame> VideoGame { get; set; }

        public virtual DbSet<VideoGameLibrary> VideoGameLibrary { get; set; }
    }
}