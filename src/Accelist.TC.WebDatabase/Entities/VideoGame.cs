using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Entities
{
    public class VideoGame
    {
        public byte[] Image { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public DateTime ReleaseDate { get; set; }

        [Key]
        public int VideoGameId { get; set; }
    }
}
