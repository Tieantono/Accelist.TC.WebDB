using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Entities
{
    public class VideoGameLibrary
    {
        public string Email { get; set; }

        public int VideoGameId { get; set; }

        [Key]
        public int VideoGameLibraryId { get; set; }
    }
}
