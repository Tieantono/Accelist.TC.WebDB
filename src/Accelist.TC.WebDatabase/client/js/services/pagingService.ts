﻿import * as $ from 'jquery';
import * as mustache from 'mustache';

class VideoGameModel {
    name: string;
    price: number;
    releaseDate: Date;
    videoGameId: number;
}

export class PagingService {
    VideoGameModel: VideoGameModel[] = Array(new VideoGameModel());

    PageButtonOnClick() {
        $("[id^=page-btn-]").click(Q => {
            let pageNumber: string = $(Q.currentTarget).attr("page-number");
            console.log(pageNumber.toString());
            $.ajax({
                url: "/Player/GetPage/" + pageNumber,
                method: "GET"
            }).then((data) => {
                $("#VideoGameList").empty();
                this.VideoGameModel = data;
                console.log(this.VideoGameModel);
                this.AppendNewVideoGameList(this.VideoGameModel, parseInt(pageNumber));
            });
        });
    }

    AppendNewVideoGameList(videoGameModel: VideoGameModel[], pageNumber: number) {
        let rowCount = ((pageNumber - 1) * 5) + 1;
        let tableBodyTemplate: string = "";
        videoGameModel.forEach((videoGame) => {
            let data = {
                rowIndex: rowCount,
                name: videoGame.name,
                price: videoGame.price,
                releaseDate: videoGame.releaseDate.toString(),
                imageUrl: '/VideoGame/GetVideoGameImage/' + videoGame.videoGameId,
                videoGameId: videoGame.videoGameId
            };
            let rowIndex = '<td>{{rowIndex}}</td>';
            let name = '<td>{{name}}</td>';
            let price = '<td>{{price}}</td>';
            let releaseDate = '<td>{{releaseDate}}</td>';
            let imageUrl = '<td><img src="{{imageUrl}}" /></td>';
            let editButton = '<td><a href="/VideoGame/Edit/{{videoGameId}}" class="btn btn-success">Edit</a></td>';
            let deleteButton = '<td><a href="/VideoGame/Delete/{{videoGameId}}" class="btn btn-danger">Delete</a></td>'
            let tableRowTemplate = '<tr>' + rowIndex + name + price + releaseDate + imageUrl + editButton + deleteButton + '</tr>';
            tableBodyTemplate = tableBodyTemplate + mustache.to_html(tableRowTemplate, data);
            rowCount++;
        });
        $("#VideoGameList").append(tableBodyTemplate);
    }
}