﻿using Accelist.TC.WebDatabase.Entities;
using Accelist.TC.WebDatabase.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Services
{
    public class RegisterService
    {
        public RegisterService(AccelistTCDbContext accelistTCDbContext, SecurityService securityService)
        {
            this.AccelistTCDbContext = accelistTCDbContext;
            this.SecurityService = securityService;
        }

        private readonly AccelistTCDbContext AccelistTCDbContext;
        private readonly SecurityService SecurityService;

        public async Task<string> RegisterNewPlayer(RegisterViewModel registerViewModel)
        {
            var newPlayer = new Player
            {
                Email = registerViewModel.Email,
                Password = this.SecurityService.EncryptPassword(registerViewModel.Password)
            };
            this.AccelistTCDbContext.Player.Add(newPlayer);
            await this.AccelistTCDbContext.SaveChangesAsync();
            return newPlayer.Email;
        }
    }
}
