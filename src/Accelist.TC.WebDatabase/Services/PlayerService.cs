﻿using Accelist.TC.WebDatabase.Entities;
using Accelist.TC.WebDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Services
{
    public class PlayerService
    {
        public PlayerService(AccelistTCDbContext accelistTCDbContext, 
            VideoGameService videoGameService)
        {
            this.AccelistTCDbContext = accelistTCDbContext;
            this.VideoGameService = videoGameService;
        }

        private readonly AccelistTCDbContext AccelistTCDbContext;
        private readonly VideoGameService VideoGameService;

        /// <summary>
        /// Fill PlayerPage
        /// </summary>
        /// <returns></returns>
        public async Task<PlayerViewModel> GetPlayerPage(string email)
        {
            var playerPage = new PlayerViewModel();
            playerPage.Email = email;
            playerPage.VideoGames = await VideoGameService
                .GetAllVideoGames(playerPage.Email);
            playerPage.TotalPage = (int)Math
                .Ceiling(await VideoGameService.CountAllVideoGames(email) / 5.0);
            return playerPage;
        }

        public async Task<List<VideoGame>> GetPlayerPageByPageNumber(int pageNumber, string email)
        {
            var videoGames = await VideoGameService.GetAllVideoGamesByPageNumber(pageNumber, email);
            return videoGames;
        }
    }
}
