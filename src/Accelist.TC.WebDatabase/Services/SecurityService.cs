﻿using Accelist.TC.WebDatabase.Entities;
using Accelist.TC.WebDatabase.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Services
{
    public class SecurityService
    {
        public SecurityService(AccelistTCDbContext accelistTCDbContext)
        {
            this.AccelistTCDbContext = accelistTCDbContext;
        }

        private readonly AccelistTCDbContext AccelistTCDbContext;

        public string EncryptPassword(string password)
        {
            var salted = BCrypt.Net.BCrypt.GenerateSalt(12);
            var hashed = BCrypt.Net.BCrypt.HashPassword(password, salted);
            return hashed;
        }

        public async Task<bool> IsRegisteredPlayer(LoginViewModel loginViewModel)
        {
            var player = await this.AccelistTCDbContext.Player
                .Where(Q => Q.Email == loginViewModel.Email)
                .FirstOrDefaultAsync();
            if (player == null)
            {
                return false;
            }
            if(this.VerifyPassword(player.Password, loginViewModel.Password) == false)
            {
                return false;
            }
            return true;
        }

        public bool VerifyPassword(string storedPassword, string submittedPassword)
        {
            if(BCrypt.Net.BCrypt.Verify(submittedPassword, storedPassword) == false)
            {
                return false;
            }
            return true;
        }

        public ClaimsPrincipal SetClaims(string email)
        {
            var claimsPrincipal = new ClaimsPrincipal();
            var claimsIdentity = new ClaimsIdentity("AccelistTrainingCenter");
            var usernameClaim = new Claim(ClaimTypes.Email, email);
            claimsIdentity.AddClaim(usernameClaim);
            claimsPrincipal.AddIdentity(claimsIdentity);
            return claimsPrincipal;
        }
    }
}
