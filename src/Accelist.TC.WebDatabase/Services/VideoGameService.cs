﻿using Accelist.TC.WebDatabase.Entities;
using Accelist.TC.WebDatabase.Models;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.WebDatabase.Services
{
    public class VideoGameService
    {
        public VideoGameService(AccelistTCDbContext accelistTCDbContext)
        {
            this.AccelistTCDbContext = accelistTCDbContext;
        }

        private readonly AccelistTCDbContext AccelistTCDbContext;

        /// <summary>
        /// Get the list of all video games based on PlayerId
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public async Task<List<VideoGame>> GetAllVideoGames(string email)
        {
            var videoGames = (await AccelistTCDbContext.
                Database.GetDbConnection().QueryAsync<VideoGame>(@"
SELECT TOP 5 vg.VideoGameId, vg.Name, vg.Price, vg.ReleaseDate
FROM VideoGameLibrary vgl
JOIN VideoGame vg ON vgl.VideoGameId = vg.VideoGameId
WHERE vgl.Email = @email
ORDER BY vg.Name
", new { email = email })).ToList();
            return videoGames;
        }
        
        public async Task<int> CountAllVideoGames(string email)
        {
            var totalVideoGame = await AccelistTCDbContext.VideoGameLibrary
                .CountAsync(Q => Q.Email == email);
            return totalVideoGame;
        }

        public async Task AddNewVideoGame(CreateVideoGameViewModel createVideoGameViewModel, string email)
        {
            var imageBytes = await this.ConvertVideoGameImageToByteArray(createVideoGameViewModel.Image);

            //
            using(var transaction = await AccelistTCDbContext.Database.BeginTransactionAsync())
            {
                var newVideoGame = new VideoGame
                {
                    Name = createVideoGameViewModel.Name,
                    Price = createVideoGameViewModel.Price,
                    ReleaseDate = createVideoGameViewModel.ReleaseDate,
                    Image = imageBytes
                };

                AccelistTCDbContext.VideoGame.Add(newVideoGame);
                await AccelistTCDbContext.SaveChangesAsync();

                var newVideoGameLibrary = new VideoGameLibrary
                {
                    Email = email,
                    VideoGameId = newVideoGame.VideoGameId
                };

                AccelistTCDbContext.VideoGameLibrary.Add(newVideoGameLibrary);
                await AccelistTCDbContext.SaveChangesAsync();

                // Don't forget to invoke the Commit() method so your changes will 
                // be saved in the database
                transaction.Commit();
            }
        }

        public async Task<byte[]> ConvertVideoGameImageToByteArray(IFormFile videoGameImage)
        {
            var imageBytes = new byte[] { 0 };
            using (var ms = new MemoryStream())
            {
                await videoGameImage.CopyToAsync(ms);
                imageBytes = ms.ToArray();
            }
            return imageBytes;
        }

        public async Task<byte[]> GetVideoGameImageByVideoGameId(int videoGameId)
        {
            var imageBytes = await AccelistTCDbContext.VideoGame
                .Where(Q => Q.VideoGameId == videoGameId)
                .Select(Q => Q.Image)
                .FirstOrDefaultAsync();
            return imageBytes;
        }

        public async Task DeleteVideoGame(int videoGameId, string email)
        {
            var deletedVideoGameLibrary = await AccelistTCDbContext.VideoGameLibrary
                .FirstOrDefaultAsync(Q => Q.VideoGameId == videoGameId && Q.Email == email);
            AccelistTCDbContext.VideoGameLibrary.Remove(deletedVideoGameLibrary);
            await AccelistTCDbContext.SaveChangesAsync();
        }

        public async Task<EditVideoGameViewModel> GetVideoGameDetail(int videoGameId)
        {
            var videoGameDetail = await AccelistTCDbContext.VideoGame
                .AsNoTracking()
                .Where(Q => Q.VideoGameId == videoGameId)
                .Select(Q => new EditVideoGameViewModel {
                    VideoGameId = Q.VideoGameId,
                    Name = Q.Name,
                    Price = Q.Price,
                    ReleaseDate = Q.ReleaseDate})
                .FirstOrDefaultAsync();
            return videoGameDetail;
        }

        public async Task<List<VideoGame>> GetAllVideoGamesByPageNumber(int pageNumber, string email)
        {
            var offset = pageNumber - 1;
            var offsetTotal = offset * 5;
            var videoGames = (await AccelistTCDbContext.
                Database.GetDbConnection().QueryAsync<VideoGame>(@"
SELECT vg.VideoGameId, vg.Name, vg.Price, vg.ReleaseDate
FROM VideoGameLibrary vgl
JOIN VideoGame vg ON vgl.VideoGameId = vg.VideoGameId
WHERE vgl.Email = @email
ORDER BY vg.Name
OFFSET @offsetTotal ROWS
FETCH NEXT 5 ROWS ONLY", new { email = email, offsetTotal = offsetTotal })).ToList();
            return videoGames;
        }

        public async Task UpdateVideoGame(EditVideoGameViewModel editedVideoGame)
        {
            var videoGame = new VideoGame
            {
                VideoGameId = editedVideoGame.VideoGameId,
                Name = editedVideoGame.Name,
                Price = editedVideoGame.Price,
                ReleaseDate = editedVideoGame.ReleaseDate
            };

            var oldVideoGame = await AccelistTCDbContext.VideoGame
                .AsNoTracking()
                .FirstOrDefaultAsync(Q => Q.VideoGameId == editedVideoGame.VideoGameId);

            if (editedVideoGame.Image == null)
            {
                videoGame.Image = oldVideoGame.Image;
            }
            else
            {
                videoGame.Image = await ConvertVideoGameImageToByteArray(editedVideoGame.Image);
            }

            this.AccelistTCDbContext.VideoGame.Update(videoGame);
            await AccelistTCDbContext.SaveChangesAsync();
        }
    }
}
